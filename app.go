package main

import (
	"siakun/app/config"
	"siakun/app/database"
	"siakun/app/logger"
	"siakun/app/routes"
	"siakun/app/utils"

	"flag"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/etag"
	"github.com/gofiber/fiber/v2/middleware/pprof"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/helmet/v2"
)

func main() {
	// Read configuration from the environment variables
	config.LoadConfigFromEnv()

	// Zap logger
	logger.SetupLogger()

	// Connecting Database
	database.Connect()

	// Parse command-line flags
	migrate := flag.Bool("migrate", false, "Check the migration request")
	flag.Parse()

	if *migrate {
		// Database Migration
		database.Migrate()
	} else {

		var prod bool = false
		if config.App.Env == "production" {
			prod = true
		}

		app := fiber.New(fiber.Config{
			Prefork:               prod,
			Concurrency:           256 * 1024 * 1024,
			ServerHeader:          config.App.Server.Name,
			BodyLimit:             config.App.Server.UploadSize,
			ReduceMemoryUsage:     true,
			DisableStartupMessage: false,
			ProxyHeader:           config.App.Server.ProxyHeader,
		})

		// Middleware
		app.Use(
			helmet.New(),
			cors.New(cors.Config{
				AllowCredentials: true,
			}),
			recover.New(),
			etag.New(),
			compress.New(compress.Config{
				Level: 1,
			}),
		)

		routes.Setup(app)

		// Prepare a static middleware to serve the built React files.
		app.Static("/siakun", "./static/build")

		// Prepare a fallback route to always serve the 'index.html', had there not be any matching routes.
		app.Static("/siakun/*", "./static/build/index.html")

		// Handle page not found
		app.Use(utils.HttpNotFound)

		if config.App.Debug {
			app.Use(pprof.New())
		}

		// Default Listen on port 3000
		app.Listen(config.App.Server.Host + ":" + config.App.Server.Port)
	}
}
