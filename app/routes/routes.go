package routes

import (
	"siakun/app/config"
	"siakun/app/controllers"
	"siakun/app/utils"

	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/gofiber/fiber/v2"
)

func Setup(app *fiber.App) {

	// Swagger router
	app.Get("/api-docs/*", swagger.HandlerDefault)

	app.Post("/register", controllers.ValidateRegisterRequest, controllers.Register)
	app.Post("/login", controllers.ValidateLoginRequest, controllers.Login)
	app.Post("/logout", controllers.Logout)

	var apiV1 = app.Group("api/v1")

	// JWT Middleware
	apiV1.Use(utils.JWTAuthenticate(&config.App))

	apiV1.Get("/user", controllers.User)

	//student Crud
	apiV1.Get("/students", controllers.GetStudents)
	apiV1.Post("/students/create", controllers.CreateStudent)
	apiV1.Get("/students/:id", controllers.GetStudent)
	apiV1.Put("/students/update/:id", controllers.UpdateStudent)
	apiV1.Delete("/students/delete/:id", controllers.DeleteStudent)

	//subject Crud
	apiV1.Get("/subjects", controllers.GetSubjects)
	apiV1.Post("/subjects/create", controllers.CreateSubject)
	apiV1.Delete("/subjects/delete/:id", controllers.DeleteSubject)
}
