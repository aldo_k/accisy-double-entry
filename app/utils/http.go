package utils

import (
	"github.com/gofiber/fiber/v2"
)

// NotFound returns custom 404 page
func HttpNotFound(c *fiber.Ctx) error {
	return c.Status(404).JSON(fiber.Map{
		"status":  false,
		"message": "Page Not Found",
	})
}
