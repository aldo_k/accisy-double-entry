package entity

import (
	"gorm.io/gorm"
)

type Setting struct {
	ID        uint
	Key       string  `gorm:"type:varchar(255)" json:"key"`
	Value     string  `gorm:"type:varchar(255)" json:"value"`
	UserId    uint    `json:"user_id"`
	CompanyId uint    `json:"company_id"`
	Company   Company `gorm:"foreignKey:CompanyId;references:id"`
	User      User    `gorm:"foreignKey:UserId;references:id"`
	gorm.Model
}
