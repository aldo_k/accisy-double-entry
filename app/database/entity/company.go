package entity

import (
	"database/sql/driver"
	"time"

	"gorm.io/gorm"
)

type Company struct {
	ID      uint
	Name    string     `gorm:"size:255; not null" json:"name"`
	Address string     `gorm:"size:255;" json:"address"`
	Email   string     `gorm:"size:255;" json:"email"`
	Web     string     `gorm:"size:255;" json:"web"`
	Phone   string     `gorm:"size:20;" json:"phone"`
	Fiscal  fiscalType `gorm:"type:ENUM('March', 'June', 'September', 'December')" json:"fiscal"`
	Incorp  time.Time  `gorm:"type:DATE" json:"incorp"`
	Enabled bool       `json:"enabled"`
	gorm.Model
}

type fiscalType string

const (
	March     fiscalType = "March"
	June      fiscalType = "June"
	September fiscalType = "September"
	December  fiscalType = "December"
)

func (ct *fiscalType) Scan(value interface{}) error {
	*ct = fiscalType(value.([]byte))
	return nil
}

func (ct fiscalType) Value() (driver.Value, error) {
	return string(ct), nil
}
