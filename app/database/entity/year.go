package entity

import (
	"time"

	"gorm.io/gorm"
)

type Year struct {
	ID        uint
	Begin     time.Time `json:"begin"`
	End       time.Time `json:"end"`
	Enabled   bool      `json:"enabled"`
	CompanyId uint      `json:"company_id"`
	Closed    bool      `json:"closed"`
	Company   Company   `gorm:"foreignKey:CompanyId;references:id"`
	gorm.Model
}
