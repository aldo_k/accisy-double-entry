package entity

import "gorm.io/gorm"

type Student struct {
	ID       uint
	Name     string    `json:"name"`
	Email    string    `gorm:"unique" json:"email"`
	Subjects []Subject `gorm:"foreignKey:StudentId"`
	gorm.Model
}
