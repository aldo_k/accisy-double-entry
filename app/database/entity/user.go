package entity

import (
	"siakun/app/config"
	"siakun/app/logger"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"gorm.io/gorm"
)

type User struct {
	ID                     uint
	Name                   string     `gorm:"size:255; not null" json:"name"`
	Email                  string     `gorm:"unique" json:"email"`
	EmailVerifiedAt        *time.Time `json:"email_verified_at"`
	Password               string     `gorm:"size:255; not null" json:"password"`
	TwoFactorSecret        string     `gorm:"type:text" json:"two_factor_secret"`
	TwoFactorRecoveryCodes string     `gorm:"type:text" json:"two_factor_recovery_codes"`
	RememberToken          string     `gorm:"type:varchar(100);unique_index" json:"remember_token"`
	CurrentTeamId          uint32     `json:"current_team_id"`
	ProfilePhotoPath       string     `gorm:"type:text" json:"profile_photo_path"`
	gorm.Model
}

type JWTToken struct {
	Hash   string `json:"access_token"`
	Expire int64  `json:"expires_in"`
}

type UserLoginForm struct {
	Email    string `json:"email" form:"email" validate:"required"`
	Password string `json:"password" form:"password" validate:"required"`
}

type UserRegisterForm struct {
	Email     string `json:"email" form:"email" validate:"required"`
	Password  string `json:"password" form:"password" validate:"required"`
	CPassword string `json:"c_password" form:"c_password" validate:"required|eq_field:password"`
}

// Create the user JWT token.
func (u *User) CreateJWTToken(secret string, expires ...int64) *JWTToken {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	expire := config.App.JWT.Expire

	if len(expires) > 0 {
		expire = expires[0]
	}

	expiresIn := time.Now().Add(time.Duration(expire) * time.Second).Unix()

	claims["iss"] = u.ID
	claims["email"] = u.Email
	claims["exp"] = expiresIn

	tokenHash, err := token.SignedString([]byte(secret))

	if err != nil {
		logger.Error(err.Error())
		panic(err)
	}

	return &JWTToken{
		Hash:   tokenHash,
		Expire: expiresIn,
	}
}
