package entity

import "gorm.io/gorm"

type Subject struct {
	ID        uint
	StudentId string `json:"student_id"`
	Name      string `json:"name"`
	gorm.Model
}
