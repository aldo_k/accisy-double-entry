package repository

import (
	"siakun/app/database/entity"
	"siakun/app/logger"

	"gorm.io/gorm"
)

type UserRepository struct {
	DB *gorm.DB
}

type UserRepositoryInterface interface {
	GetByEmail(email string) *entity.User
	Create(user *entity.User) *entity.User
}

// Create a new user repository instance.
func NewUser(orm *gorm.DB) UserRepositoryInterface {
	return &UserRepository{
		DB: orm,
	}
}

// Get the user by email.
func (repo *UserRepository) GetByEmail(email string) *entity.User {
	var user entity.User

	if err := repo.DB.Where(&entity.User{Email: email}).First(&user).Error; err != nil {
		logger.Error(err.Error())
		panic(err)
	}

	return &user
}

// Create a new user.
func (repo *UserRepository) Create(user *entity.User) *entity.User {
	repo.DB.Create(&user)

	return user
}
