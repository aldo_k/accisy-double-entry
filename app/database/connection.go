package database

import (
	"fmt"
	"siakun/app/config"
	"siakun/app/database/entity"
	"siakun/app/logger"
	"time"

	"gorm.io/gorm"

	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm/schema"
	"gorm.io/plugin/dbresolver"
	"moul.io/zapgorm2"
)

var DB *gorm.DB

func Connect() {
	var (
		err error
		dsn string
		cfg *gorm.Config
	)

	loggers := zapgorm2.New(logger.Zap)
	loggers.SetAsDefault()

	cfg = &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix:   config.App.DB.TablePrefix,
			SingularTable: false,
		},
		Logger: loggers,
	}

	switch config.App.DB.Driver {
	case "mysql":
		dsn = fmt.Sprintf(
			"%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
			config.App.DB.Username,
			config.App.DB.Password,
			config.App.DB.Host,
			config.App.DB.Port,
			config.App.DB.DBName,
		)

		DB, err = gorm.Open(mysql.Open(dsn), cfg)
	case "postgres":
		dsn = fmt.Sprintf(
			"host=%s user=%s password=%s dbname=%s port=%d sslmode=%s TimeZone=%s",
			config.App.DB.Host,
			config.App.DB.Username,
			config.App.DB.Password,
			config.App.DB.DBName,
			config.App.DB.Port,
			config.App.DB.SSLMode,
			config.App.Timezone,
		)

		DB, err = gorm.Open(postgres.Open(dsn), cfg)
	default:
		DB, err = gorm.Open(sqlite.Open(config.App.DB.SQLiteFile), cfg)
	}

	if err != nil {
		logger.Error(err.Error())
		panic(err)
	}

	DB.Use(
		dbresolver.Register(dbresolver.Config{}).
			SetConnMaxLifetime(24 * time.Hour).
			SetMaxIdleConns(100).
			SetMaxOpenConns(100),
	)
}

// Execute the DB migration.
func Migrate() {
	logger.Info("Initiating migration...")

	err := DB.Migrator().AutoMigrate(
		&entity.User{},
		&entity.Student{},
		&entity.Subject{},
		&entity.Company{},
		&entity.Year{},
		&entity.Setting{},
	)

	if err != nil {
		logger.Error(err.Error())
		panic(err)
	}

	logger.Info("Migration Completed.")
}
