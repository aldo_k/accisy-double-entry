package controllers

import (
	"siakun/app/database"
	"siakun/app/database/entity"

	"github.com/gofiber/fiber/v2"
)

func CreateSubject(c *fiber.Ctx) error {

	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	subject := entity.Subject{
		Name:      data["name"],
		StudentId: data["student_id"],
	}

	database.DB.Create(&subject)

	return c.JSON(subject)

}

func GetSubjects(c *fiber.Ctx) error {

	var subjects []entity.Subject

	database.DB.Find(&subjects)

	return c.JSON(subjects)
}

func DeleteSubject(c *fiber.Ctx) error {

	var subject entity.Subject

	database.DB.Where("id = ?", c.Params("id")).Delete(&subject)

	var subjects []entity.Subject

	database.DB.Find(&subjects)

	return c.JSON(subjects)
}
