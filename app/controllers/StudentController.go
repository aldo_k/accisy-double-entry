package controllers

import (
	"siakun/app/database"
	"siakun/app/database/entity"

	"github.com/gofiber/fiber/v2"
)

func CreateStudent(c *fiber.Ctx) error {

	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	student := entity.Student{
		Name:  data["name"],
		Email: data["email"],
	}

	database.DB.Create(&student)

	return c.JSON(student)

}

func GetStudents(c *fiber.Ctx) error {

	var students []entity.Student

	database.DB.Preload("Subjects").Find(&students)

	return c.JSON(students)
}

func GetStudent(c *fiber.Ctx) error {

	var student entity.Student

	database.DB.Where("id = ?", c.Params("id")).First(&student)

	return c.JSON(student)
}

func UpdateStudent(c *fiber.Ctx) error {

	var student entity.Student

	database.DB.Where("id = ?", c.Params("id")).First(&student)

	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	student.Name = data["name"]
	student.Email = data["email"]

	database.DB.Save(&student)

	return c.JSON(student)
}

func DeleteStudent(c *fiber.Ctx) error {

	var student entity.Student

	database.DB.Where("id = ?", c.Params("id")).Delete(&student)

	var students []entity.Student

	database.DB.Find(&students)

	return c.JSON(students)
}
