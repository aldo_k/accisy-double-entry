package controllers

import (
	"time"

	"siakun/app/config"
	"siakun/app/database"
	"siakun/app/database/entity"
	"siakun/app/database/repository"
	"siakun/app/logger"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"github.com/gookit/validate"
	"golang.org/x/crypto/bcrypt"
)

func Register(c *fiber.Ctx) error {
	user := c.Locals("user").(*entity.User)
	repo := c.Locals("repository").(repository.UserRepositoryInterface)

	repo.Create(user)

	token := user.CreateJWTToken(config.App.JWT.Secret)
	refreshToken := user.CreateJWTToken(config.App.JWT.Secret, config.App.JWT.Expire*10)

	return c.JSON(fiber.Map{
		"message": "logged In!",
		"data": fiber.Map{
			"user":                 user,
			"token":                token.Hash,
			"expire_token":         token.Expire,
			"refresh_token":        refreshToken.Hash,
			"expire_refresh_token": refreshToken.Expire,
		},
	})
}

// Validate the POST register request.
func ValidateRegisterRequest(c *fiber.Ctx) error {
	var data entity.UserRegisterForm

	if err := c.BodyParser(&data); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	v := validate.Struct(data)

	if !v.Validate() {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": v.Errors,
		})
	}

	repo := repository.NewUser(database.DB)

	record := repo.GetByEmail(data.Email)

	if record != nil {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(fiber.Map{
			"error": "User already exists",
		})
	}

	var user entity.User

	user.Email = data.Email
	user.Password = CreatePassword(data.Password)

	c.Locals("user", &user)
	c.Locals("repository", repo)

	return c.Next()
}

// Create the password hash.
func CreatePassword(password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 10)

	if err != nil {
		logger.Error(err.Error())
		panic(err)
	}

	return string(hash)
}

func Login(c *fiber.Ctx) error {
	user := c.Locals("user").(*entity.User)
	token := user.CreateJWTToken(config.App.JWT.Secret)
	refreshToken := user.CreateJWTToken(config.App.JWT.Secret, config.App.JWT.Expire*10)

	return c.JSON(fiber.Map{
		"message": "logged In!",
		"data": fiber.Map{
			"user":                 user,
			"token":                token.Hash,
			"expire_token":         token.Expire,
			"refresh_token":        refreshToken.Hash,
			"expire_refresh_token": refreshToken.Expire,
		},
	})

}

// Validate the POST login request.
func ValidateLoginRequest(c *fiber.Ctx) error {
	var data entity.UserLoginForm

	if err := c.BodyParser(&data); err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	v := validate.Struct(data)

	if !v.Validate() {
		logger.Error(v.Errors.String())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": v.Errors,
		})
	}

	repo := repository.NewUser(database.DB)
	user := repo.GetByEmail(data.Email)

	if user == nil {
		logger.Error("User not found.")
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": "User not found.",
		})
	}

	match := ComparePasswordHash(data.Password, user.Password)

	if !match {
		logger.Error("Invalid email or password.")
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": "Invalid email or password.",
		})
	}

	c.Locals("user", user)

	return c.Next()
}

// Compares a bcrypt hashed password with user password.
func ComparePasswordHash(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))

	return err != nil
}

func User(c *fiber.Ctx) error {
	cookie := c.Cookies("jwt")

	token, err := jwt.ParseWithClaims(cookie, &jwt.StandardClaims{}, func(t *jwt.Token) (interface{}, error) {
		return []byte(config.App.JWT.Secret), nil
	})

	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthenticated",
		})
	}

	claims := token.Claims.(*jwt.StandardClaims)

	var user entity.User

	database.DB.Where("id = ?", claims.Issuer).First(&user)

	return c.JSON(user)
}

func Logout(c *fiber.Ctx) error {
	cookie := fiber.Cookie{
		Name:    "jwt",
		Value:   "",
		Expires: time.Now().Add(-time.Hour),
	}

	c.Cookie(&cookie)

	return c.JSON(fiber.Map{
		"message": "Logged Out!",
	})

}
